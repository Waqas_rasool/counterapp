import React, { useState } from 'react';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import BackspaceIcon from '@material-ui/icons/Backspace';
//Fat arrow function
const App = () =>{
const state = useState();

//Array Destructuring && Hooks function
const [count,setCount] = useState(0); 

const IncNum = () => {

    setCount(count + 1);

};
const DecNum = () => {
    if (count > 0){
    setCount(count - 1);
        
}else {
    alert('Sorry ! Limit is Zero')
    setCount(0);
}

};
    return (
        <>
         <div className = 'main_div'> 
            <div className = 'center_div'>
        <h1> {count} </h1>
        <div className= "btn_div">
            <Button className= "btn_green" variant="contained"  onClick = {IncNum}> <AddIcon /> </Button>
            <Button className= "btn_red"  variant="contained"  onClick = {DecNum}>  <BackspaceIcon/></Button>
            
        </div>
        
        </div>
        </div>
        </>
    )
}

export default App;
